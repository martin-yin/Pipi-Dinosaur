import Vue from 'vue'
import App from './App.vue'
import router from './router'

import { Tabbar, TabbarItem, Dialog } from '../packages';
Vue.component(TabbarItem.name, TabbarItem);
Vue.component(Tabbar.name, Tabbar);

Vue.component(Dialog.name, Dialog);



Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
