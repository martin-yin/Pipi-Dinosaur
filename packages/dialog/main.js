import Popup from '../popup';
export default({
    name: 'pi-dialog',
    data() {
        return {
          inited: this.value
        };
    },
    props: {
        value: Boolean,
        title: String,
        message: String,
        cancelButtonText: String,
        cancelButtonColor: String,
        confirmButtonText: String,
        confirmButtonColor: String,
        showCancelButton: Boolean,
        showConfirmButton: {
          type: Boolean,
          default: true
        },
        callback: Function
    },
    watch: {
        value() {
          this.inited = this.inited || this.value;
        }
    },
    computed: {
        shouldRender() {
          return this.inited;
        }
    },
    methods: {
        handleAction(action) {
            this.close(action);
        },
        close(action) {
            if (this.callback) {
                this.value = false;
                this.callback(action);
            } else {
                this.$emit('input', false);
            }
        },
    },
    render() {
        const { title }  = this;
     
        if (!this.shouldRender) {
            return;
        }
        const childRen = this.$slots.default;
        const Title = title ? <div class="diglog-title">{title}</div> : null;
        const Content = ( childRen || this.message ) && (<div>{childRen || <div class="dialog-content">{this.message}</div>}</div>);
        const Buttton = (<div class="dialog-footer">
                {this.showCancelButton && (
                    <div class="dialog-btn" onClick={() => {
                        this.handleAction('cancel');
                    }}>取消</div>
                )}{this.showConfirmButton && (
                    <div class="dialog-btn" onClick={() => {
                        this.handleAction('confirm');
                    }}>确定</div>
                )}
        </div>);
        return(
            <Popup value={this.value} position="center">
                <div class="dialog">
                    {Title}
                    {Content}
                    {Buttton}
                </div>
            </Popup>
        )
    }
});