const positionList = {
    'center': '',
    'bottom': 'bottom-modal',
    'left': 'drawer-modal justify-start',
    'right': 'drawer-modal justify-end'
  }
  
  export default({
    name: 'pi-popup',
    props: {
      value: Boolean,
      position: {
        default: 'center',
        type: String
      },
      styleed: String
    },
    data() {
      return {
        inited: this.value
      };
    },
    watch: {
      value(val) {
        const type = val ? 'open' : 'close';
        this.inited = this.inited || this.value;
        this[type]();
        this.$emit(type);
      }
    },
    computed: {
      positioned() {
        return positionList[this.position];
      }
    },
    methods: {
      open(){
        this.$emit('input', true);
      },
      close(){
        this.$emit('input', false);
      } 
    },
    render() {
      const { $slots, value, styleed } = this;
      if(!value){
        return;
      }
      const classed = [
        'popup',
        { 'show': value === true },
        this.positioned
      ];
      return(
        <div class={classed}>
          <div style={styleed}>
          {$slots.default}
          </div>
        </div>
      )
    }
  });