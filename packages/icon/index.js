export default {
    name: 'PiIcon',
    props: {
        icon: String,
        size: String,
        color: String,
    },
    render() {
        const { icon, color, size } = this;
        const styleed = {
            'font-size': size,
            'color': color,
        }
        return (
            <span style={styleed} class={[icon]}></span>
        )
    }
}