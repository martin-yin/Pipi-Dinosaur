
// import Vue from 'vue';
import Tabbar from './tabbar';
import TabbarItem from './tabbar-item';
import Icon from './icon';
import Tag from './tag';
import Cell from './cell';
import CellGroup from './cell-group';
import NavBar from './nav-bar';
import Popup from './popup';
import Dialog from './dialog';

const version = '0.0.1';
const components = [
    Tabbar,
    TabbarItem,
    Icon,
    Tag,
    Cell,
    CellGroup,
    NavBar,
    Popup,
    Dialog,
];


const install = function(Vue) {
  if (install.installed) return
  components.map(component => Vue.component(component.name, component))
  Vue.prototype.$dialog = Dialog;
}

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}
export default {
  version,
  install
};

export {
  version,
  Tabbar,
  TabbarItem,
  Icon,
  Tag,
  Cell,
  CellGroup,
  NavBar,
  Popup,
  Dialog,
};